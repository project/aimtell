<?php

/**
 * @file
 * Default output for the AimTell module's initialization code.
 */

/**
 * Available variables:
 * $domain
 * $owner
 * $site_id
 * $webpush_id
 */
?>
<!-- Start Webpush tracking code -->
<script type='text/javascript'>var _at = {}; window._at.track = window._at.track || function(){(window._at.track.q = window._at.track.q || []).push(arguments);}; _at.domain = '<?php print $domain ?>';_at.owner = '<?php print $owner; ?>';_at.idSite = '<?php print $site_id; ?>';_at.attributes = {};_at.webpushid = '<?php print $webpush_id; ?>';(function() { var u='//s3.amazonaws.com/cdn.aimtell.com/trackpush/'; var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'trackpush.min.js'; s.parentNode.insertBefore(g,s); })();</script>
<!-- End Webpush tracking code -->
