<?php

/**
 * @file
 * Primary hook implementations for the AimTell module.
 */

/**
 * Implements hook_init().
 */
function aimtell_init() {
  $args = array(
    'domain' => check_plain(variable_get('aimtell_domain')),
    'owner' => check_plain(variable_get('aimtell_owner')),
    'site_id'=> check_plain(variable_get('aimtell_site_id')),
    'webpush_id' => check_plain(variable_get('aimtell_webpush_id')),
  );

  // Only proceed if all variables are present.
  $args = array_filter($args);
  if (count($args) == 4) {
    $element = array(
      '#type' => 'markup',
      '#markup' => theme('aimtell_code', $args),
    );
    drupal_add_html_head($element, 'aimtell');
  }
}

/**
 * Implements hook_menu().
 */
function aimtell_menu() {
  $items = array();

  $items['aimtell-worker.js'] = array(
    'title' => 'AimTell',
    'description' => 'Configure the AimTell integration.',
    'page callback' => 'aimtell_worker_js',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'aimtell.pages.inc',
  );

  $items['admin/config/services/aimtell'] = array(
    'title' => 'AimTell',
    'description' => 'Configure the AimTell integration.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('aimtell_settings_form'),
    'access callback' => 'user_access',
    'access arguments' => array('administer aimtell'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'aimtell.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function aimtell_permission() {
  return array(
    'administer aimtell' => array(
      'title' => 'Administer AimTell integration',
    ),
  );
}

/**
 * Implements hook_theme().
 */
function aimtell_theme() {
  return array(
    'aimtell_code' => array(
      'template' => 'aimtell-code',
      'variables' => array(
        'pid' => 0,
        'zone' => NULL,
      ),
    ),
  );
}
