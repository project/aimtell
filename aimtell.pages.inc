<?php

/**
 * @file
 * Additional hook_menu callbacks for the AimTell module.
 */

/**
 * Output the aimtell-worker.js content.
 */
function aimtell_worker_js() {
  print "importScripts('https://cdn.aimtell.com/sdk/aimtell-worker-sdk.js');\n";
  return;
}
