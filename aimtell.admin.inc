<?php

/**
 * @file
 * Admin functionality for the AimTell module.
 */

/**
 * FormAPI callback for the AimTell settings page.
 */
function aimtell_settings_form() {
  $form['aimtell_domain'] = array(
    '#type' => 'textfield',
    '#title' => 'Site hostname',
    '#description' => t('The full hostname of the website. Do not include the "http://" or "https://" portion.'),
    '#default_value' => variable_get('aimtell_domain'),
    '#required' => TRUE,
  );

  $form['aimtell_owner'] = array(
    '#type' => 'textfield',
    '#title' => 'Site owner',
    '#description' => t('The alphanumerical site owner ID.'),
    '#default_value' => variable_get('aimtell_owner'),
    '#required' => TRUE,
  );

  $form['aimtell_site_id'] = array(
    '#type' => 'textfield',
    '#title' => 'Site ID',
    '#description' => t('The numerical site ID.'),
    '#default_value' => variable_get('aimtell_site_id'),
    '#required' => TRUE,
  );

  $form['aimtell_webpush_id'] = array(
    '#type' => 'textfield',
    '#title' => 'Web Push ID',
    '#description' => t('The hostname that will be used, e.g. "web.70.aimtell.com".'),
    '#default_value' => variable_get('aimtell_webpush_id'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
